﻿CREATE TABLE [dbo].[Authors]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Name] VARCHAR(50) NOT NULL, 
    [age] INT NOT NULL, 
    [designation] VARCHAR(50) NOT NULL, 
    [salary] NCHAR(10) NOT NULL
)
