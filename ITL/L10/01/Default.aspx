﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">

        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ConnectionStrings:L10%>"
            SelectCommand="select * from Authors" />
        <asp:GridView ID="GridView1" runat="server" DataSourceID="SqlDataSource1" AllowPaging="true" PageSize="5"
            AutoGenerateColumns="false">
            <Columns>
                <asp:TemplateField SortExpression="name" HeaderText="Staff Details">
                    <ItemTemplate>
                        <%#Eval("name")%><br />
                        <%#Eval("age")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="designation" HeaderText="Designation"/>
                <asp:BoundField DataField="salary" HeaderText="Salary"/>
            </Columns>
        </asp:GridView>

    </form>
</body>
</html>
