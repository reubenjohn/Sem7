﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">

        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ConnectionStrings:L10 %>" SelectCommand="select * from Authors2" />
        <asp:GridView ID="GridView1" runat="server" DataSourceID="SqlDataSource1" AutoGenerateColumns="false">
            <Columns>
                <asp:CommandField ShowEditButton="true" />
                <asp:BoundField DataField="id" />
                <asp:BoundField DataField="title" />
            </Columns>
        </asp:GridView>

    </form>
</body>
</html>
