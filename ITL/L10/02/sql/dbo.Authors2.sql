﻿CREATE TABLE [dbo].[Authors2]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [name] VARCHAR(50) NOT NULL, 
    [title] VARCHAR(50) NOT NULL, 
    [genre] VARCHAR(50) NOT NULL, 
    [copies_sold] INT NOT NULL
)
