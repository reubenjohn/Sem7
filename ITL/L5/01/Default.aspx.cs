﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _01
{
    public partial class Form01 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            Response.Redirect(typeof(Result).Name + ".aspx?man=" + Server.UrlEncode(manufacturers.SelectedItem.Text) + "&mod=" + Server.UrlEncode(model.Value));
        }
    }
}