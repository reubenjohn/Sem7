﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="_01.Form01" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Car manufacturers:
            <asp:DropDownList runat="server" ID="manufacturers">
                <asp:ListItem Text="Toyota" Value="2" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Honda" Value="1"></asp:ListItem> 
            </asp:DropDownList>
            <br />
            Model:
            <textarea runat="server" id="model"></textarea>
            <asp:Button runat="server" ID="Submit" Text="Submit" OnClick="Submit_Click"/>
        </div>
    </form>
</body>
</html>
