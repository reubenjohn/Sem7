﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _04
{
    public partial class Result : System.Web.UI.Page
    {
        int[] laptopRates = new int[] { 10, 10, 10, 10, 10 };
        int[] mobileRates = new int[] { 10, 10, 10, 10, 10 };
        int[] comboRates = new int[] { 10, 10, 10, 10, 10 };
        public int toInt(string a)
        {
            return Convert.ToInt32(a);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if(PreviousPage != null)
            {
                Default myForm = PreviousPage as Default;
                ans.InnerHtml =
                    "Company: " + myForm.selectedCompany + "<br>" +
                    "Item: " + (myForm.laptopChosen ? "Laptop, " : "") + (myForm.mobileChosen ? "Mobile" : "") + "<br>" +
                    "Price: " + ((myForm.laptopChosen ? (myForm.mobileChosen ? comboRates[toInt(myForm.selectedCompanyVal)] : laptopRates[toInt(myForm.selectedCompanyVal)]) : mobileRates[toInt(myForm.selectedCompanyVal)]) * toInt(myForm.selectedQty)).ToString() + "<br>"
                    ;
            }
        }
    }
}