﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="_04.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        Company:
        <asp:DropDownList runat="server" ID="company">
            <asp:ListItem Value="1" Text="HP"></asp:ListItem>
            <asp:ListItem Value="2" Text="Nokia"></asp:ListItem>
            <asp:ListItem Value="3" Text="Motorola"></asp:ListItem>
            <asp:ListItem Value="4" Text="Samsung"></asp:ListItem>
            <asp:ListItem Value="5" Text="Apple"></asp:ListItem>
        </asp:DropDownList>
        <br />
        <asp:CheckBox runat="server" ID="itemMobile" Text="Mobiles" />
        <asp:CheckBox runat="server" ID="itemLaptop" Text="Laptops" />
        <br />
        Quantity:
        <asp:TextBox ID="qty" runat="server"></asp:TextBox>
        <br />
        <asp:Button ID="submit" Text="Produce bill" OnClick="submit_Click" PostBackUrl="~/Result.aspx" runat="server" />
    </form>
</body>
</html>
