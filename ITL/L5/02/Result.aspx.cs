﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _02
{
    public partial class Result : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            label1.InnerHtml = Session["One"] + "<br>" + Session["Two"] + "<br>" + Session["Three"];
        }

        protected void incr_Click(object sender, EventArgs e)
        {
            try
            {
                int curVal = Convert.ToInt32(Request.Cookies["count"].Value);
                label2.InnerText = (curVal + 1).ToString();
                Response.Cookies["count"].Value = (curVal + 1).ToString();
                Response.Cookies["count"].Expires = DateTime.Now.AddDays(1);
            }
            catch
            {
                label2.InnerText = "-1";
            }
        }
    }
}