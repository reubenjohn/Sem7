﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="_02.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <textarea id="t1" runat="server"></textarea>
            <br />
            <textarea id="t2" runat="server"></textarea>
            <br />
            <asp:DropDownList runat="server" ID="list">
                <asp:ListItem Text="One" Value="One" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Two" Value="Two"></asp:ListItem>
            </asp:DropDownList>
            <asp:Button Text="Submit" OnClick="Unnamed_Click" runat="server" />
        </div>
    </form>
</body>
</html>
