﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _02
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cookies["count"].Value = "0";
            Response.Cookies["count"].Expires = DateTime.Now.AddDays(1);
        }

        protected void Unnamed_Click(object sender, EventArgs e)
        {
            Session["One"] = t1.Value;
            Session["Two"] = t2.Value;
            Session["Three"] = list.SelectedItem;
            Response.Redirect(typeof(Result).Name+".aspx");
        }
    }
}