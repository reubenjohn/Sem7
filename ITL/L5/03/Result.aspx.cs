﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _03
{
    public partial class Result : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (PreviousPage != null)
            {
                Default myForm = PreviousPage as Default;
                name.InnerText = "Name: " + myForm.var_username;
                email.InnerText = "Email: " + myForm.var_email;
                phone.InnerText = "Phone: " + myForm.var_phone;
            }
        }
    }
}