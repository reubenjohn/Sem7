﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            DirectoryInfo diFiles = new DirectoryInfo(Server.MapPath("~/emps/"));
            FileInfo[] imgList = diFiles.GetFiles("*.jpg");
            int empId = 0;
            foreach (FileInfo fileinfo in imgList)
            {
                EmpList1.Items.Add(new ListItem(empId.ToString(), fileinfo.Name));
                empId++;
            }
            EmpList1.DataBind();
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        EmpImg.ImageUrl = "~/emps/" + EmpList1.SelectedValue;
        DateTime DateOfJoining = DateTime.ParseExact(DOJ.Text,
                        "yyyy-MM-dd", CultureInfo.InvariantCulture);
        Eligibility.Text = DateTime.Compare(DateTime.UtcNow, DateOfJoining.AddYears(5)) > 0 ? "YES" : "NO";
    }
}