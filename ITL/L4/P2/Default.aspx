﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">

        <asp:Label ID="Label1" runat="server" Text="Employee ID: "></asp:Label>
        <asp:DropDownList ID="EmpList1" runat="server" OnSelectedIndexChanged="Button1_Click" AutoPostBack="true">
        </asp:DropDownList>

        <br />
        <asp:Label ID="Label2" runat="server" Text="Image: "></asp:Label>
        <asp:Image ID="EmpImg" runat="server" Width="25%" />

        <br />
        <asp:Label ID="Label3" runat="server" Text="Employee Name: "></asp:Label>
        <asp:TextBox ID="EmpName" runat="server"></asp:TextBox>

        <br />
        <asp:Label ID="Label4" runat="server" Text="Date of Joining: "></asp:Label>

        <asp:TextBox ID="DOJ" runat="server" placeholder="yyyy-MM-dd"></asp:TextBox>
        <br />
        <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click"/>

        <br />
        <asp:Label ID="Eligibility" runat="server" Text=""></asp:Label>

    </form>
</body>
</html>
