﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            DirectoryInfo diFiles = new DirectoryInfo(Server.MapPath("~/covers/"));
            CoverImageList.DataSource = diFiles.GetFiles("*.jpg");
            CoverImageList.DataBind();

            foreach (KnownColor r in Enum.GetValues(typeof(KnownColor)))
            {
                ListItem item = new ListItem(Enum.GetName(typeof(KnownColor), r), r.ToString());
                backgroundList.Items.Add(item);
                FontColorList.Items.Add(item);
            }
        }
    }

    protected void refreshB_Click(object sender, EventArgs e)
    {
        preview.Style[HtmlTextWriterStyle.BackgroundColor] = backgroundList.SelectedItem.Value;
        CoverImage.ImageUrl = "~/covers/" + CoverImageList.SelectedItem.Text; ;
        CoverImage.Style[HtmlTextWriterStyle.BorderColor] = backgroundList.SelectedItem.Value;
        CoverBody.Style[HtmlTextWriterStyle.Color] = FontColorList.SelectedItem.Value;
        CoverBody.Text = CoverBodyInput.Value;
        int fontSize;
        if(int.TryParse(FontSizeInput.Text, out fontSize))
        {
            CoverBody.Font.Size = fontSize;
        }
    }

    protected void CoverImageList_SelectedIndexChanged(object sender, EventArgs e)
    {
        CoverImage.ImageUrl = "~/covers/" + ((DropDownList)sender).SelectedItem.Text;
    }
}