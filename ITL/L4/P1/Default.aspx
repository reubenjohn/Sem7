﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Table ID="Table1" runat="server" GridLines="Both" BorderStyle="Solid">
            <asp:TableHeaderRow runat="server">
                <asp:TableHeaderCell Width="50%" Text="Editor" runat="server">
                    <span>Editor</span>
                </asp:TableHeaderCell>
                <asp:TableHeaderCell Width="50%" Text="Preview" runat="server" />
            </asp:TableHeaderRow>
            <asp:TableRow>
                <asp:TableCell Width="50%" runat="server">

                    <asp:Label Text="Cover Image: " runat="server" />
                    <asp:DropDownList ID="CoverImageList" runat="server" OnSelectedIndexChanged="CoverImageList_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                    <br />
                    <asp:Label ID="Label1" runat="server" Text="Background: "></asp:Label>
                    <asp:DropDownList ID="backgroundList" runat="server">
                    </asp:DropDownList>

                    <br />
                    <asp:Label ID="Label2" runat="server" Text="Font Size: "></asp:Label>

                    <asp:TextBox ID="FontSizeInput" runat="server"></asp:TextBox>

                    <br />
                    <asp:Label ID="Label3" runat="server" Text="Font Color"></asp:Label>

                    <asp:DropDownList ID="FontColorList" runat="server">
                    </asp:DropDownList>

                    <br />
                    <textarea id="CoverBodyInput" cols="20" name="S1" rows="2" runat="server"></textarea><br />

                    <asp:Button ID="refreshB" runat="server" Text="Preview" OnClick="refreshB_Click" />
                </asp:TableCell>
                <asp:TableCell Width="50%" runat="server" ID="preview">
                    <asp:Image ID="CoverImage" runat="server" BorderWidth="24px" Width="25%" />
                    <br />
                    <asp:Label ID="CoverBody" runat="server" />
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <div>
        </div>
    </form>
</body>
</html>
