﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    string[] IMAGE_URLS = {
        "https://upload.wikimedia.org/wikipedia/commons/thumb/6/69/20070304%E8%B4%B5%E9%98%B3-%E9%BB%84%E8%85%B0%E6%9F%B3%E8%8E%BA_%28cropped_and_mirrored%29.jpg/133px-20070304%E8%B4%B5%E9%98%B3-%E9%BB%84%E8%85%B0%E6%9F%B3%E8%8E%BA_%28cropped_and_mirrored%29.jpg",
        "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a0/Harvey_2017-08-25_2230Z.png/100px-Harvey_2017-08-25_2230Z.png",
        "https://upload.wikimedia.org/wikipedia/commons/thumb/0/06/O%27Toole%27s_Pub%2C_The_Heights%2C_Loughinisland_-_geograph.org.uk_-_1444364.jpg/120px-O%27Toole%27s_Pub%2C_The_Heights%2C_Loughinisland_-_geograph.org.uk_-_1444364.jpg",
        "https://upload.wikimedia.org/wikipedia/commons/thumb/5/58/Nuremberg_chronicles_f_143r_3.jpg/100px-Nuremberg_chronicles_f_143r_3.jpg",
        "https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Bryum_capillare_leaf_cells.jpg/380px-Bryum_capillare_leaf_cells.jpg"
    };
    protected void Page_Load(object sender, EventArgs e)
    {
        preview.ImageUrl = IMAGE_URLS[companies.SelectedIndex];
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        int intAge, intDays;
        if (!int.TryParse(age.Text, out intAge)) intAge = -1;
        if (!int.TryParse(days.Text, out intDays)) intDays = -1;
        response.Text = (intAge > 18 && companies.SelectedIndex == 0 && intDays > 0 && intDays < 2)
            ? "Approved" : "Rejected";
    }
}