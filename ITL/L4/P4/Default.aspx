﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        Company<br />
        <asp:RadioButtonList ID="companies" runat="server" AutoPostBack="true">
            <asp:ListItem Value="1" Text="Honda" Selected="True"/>
            <asp:ListItem Value="2" Text="Hero" />
        </asp:RadioButtonList>
        Age<br />
        <asp:TextBox ID="age" runat="server"></asp:TextBox>
        <br />
        Days<br />
        <asp:TextBox ID="days" runat="server"></asp:TextBox>
        <br />
        Preview<br />
        <asp:Image ID="preview" runat="server" ImageUrl="https://upload.wikimedia.org/wikipedia/commons/thumb/6/69/20070304%E8%B4%B5%E9%98%B3-%E9%BB%84%E8%85%B0%E6%9F%B3%E8%8E%BA_%28cropped_and_mirrored%29.jpg/133px-20070304%E8%B4%B5%E9%98%B3-%E9%BB%84%E8%85%B0%E6%9F%B3%E8%8E%BA_%28cropped_and_mirrored%29.jpg"/>
        <br />
        <asp:Button ID="Button1" runat="server" Text="Request" OnClick="Button1_Click"/>
        <br />
        <asp:Label ID="response" runat="server" Text=""></asp:Label>
    </form>
</body>
</html>
