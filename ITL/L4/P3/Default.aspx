﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="Label1" runat="server" Text="Name: "></asp:Label>
        <asp:TextBox ID="Name" runat="server"></asp:TextBox>
        <br />
        <asp:Label ID="Label2" runat="server" Text="DOB: "></asp:Label>
        <asp:TextBox ID="DOB" runat="server"></asp:TextBox>
        <br />
        <asp:Label ID="Label3" runat="server" Text="Address: "></asp:Label>
        <asp:TextBox ID="Address" runat="server"></asp:TextBox>
        <br />
        <asp:Label ID="Label4" runat="server" Text="Contact: "></asp:Label>
        <asp:TextBox ID="Contact" runat="server"></asp:TextBox>
        <br />
        <asp:Label ID="Label5" runat="server" Text="Email: "></asp:Label>
        <asp:TextBox ID="Email" runat="server"></asp:TextBox>
        <br />
        <asp:Label ID="Label6" runat="server" Text="Marks [English, Physics, Chemistry]: "></asp:Label>
        <asp:TextBox ID="Marks" runat="server"></asp:TextBox>
        <br />
        <asp:Button ID="Button1" runat="server" Text="Submit" OnClick="Button1_Click" />
        <br />
        <br />
        <asp:Label ID="Label7" runat="server" Text="Summary"></asp:Label>
        <br />
        <textarea id="Summary" cols="20" name="S1" rows="10" runat="server"></textarea><br />
        <asp:Label ID="Label8" runat="server" Text="Total Marks %: "></asp:Label>
        <asp:Label ID="TotalPerc" runat="server" Text="NA"></asp:Label>
    </form>
</body>
</html>
