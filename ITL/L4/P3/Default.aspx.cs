﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        Summary.InnerText = Name.Text
            + "\n" + DOB.Text
            + "\n" + Address.Text
            + "\n" +Contact.Text
            + "\n" +Email.Text
            + "\n" +Marks.Text;
        double[] marks = Array.ConvertAll(Marks.Text.Split(','), Double.Parse);
        double total = 0;
        foreach(double mark in marks)
        {
            total += mark;
        }
        total /= 3;
        TotalPerc.Text = total.ToString();
    }
}