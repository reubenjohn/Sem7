﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Select Category :
            <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="true" DataSourceID="SqlDataSource1" DataTextField="category"></asp:DropDownList><br />
            Actors :
            <asp:ListBox ID="ListBox1" runat="server" AutoPostBack="true" DataSourceID="SqlDataSource2" DataTextField="name"></asp:ListBox><br />
            Details :
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="true" DataSourceID="sqldatasource3"></asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ConnectionStrings:Pubs %>" SelectCommand="SELECT distinct category from Test"></asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ConnectionStrings:Pubs %>" SelectCommand="SELECT name from test where category=@cat">
                <SelectParameters>
                    <asp:ControlParameter Name="cat" ControlId="DropDownList1" PropertyName="Text" />
                    </SelectParameters>
            </asp:SqlDataSource>
             <asp:SqlDataSource ID="sqldatasource3" runat="server" ConnectionString="<%$ConnectionStrings:Pubs %>" SelectCommand="SELECT * FROM TEST WHERE NAME=@name">
                                
            <SelectParameters >
                <asp:ControlParameter Name="name" ControlID="ListBox1" PropertyName="Text" />
            </SelectParameters>
             </asp:SqlDataSource>

        </div>
    </form>
</body>
</html>
