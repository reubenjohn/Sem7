﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            States:
            <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="dropdownlist1_selected"></asp:DropDownList><br />
            Cities:            
            <asp:DropDownList ID="DropDownList2" runat="server"></asp:DropDownList>
        </div>
    </form>
</body>
</html>
