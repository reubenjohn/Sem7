﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.Configuration;


public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Dictionary<int, string> states = new Dictionary<int, string>();
            states.Add(1, "Haryana");
            states.Add(2, "Karnataka");
            states.Add(3, "Maharashtra");

            DropDownList1.DataSource = states;
            DropDownList1.DataTextField = "Value";
            DropDownList1.DataValueField = "Key";
            DropDownList1.Items.Insert(0, new ListItem("Select State", "0"));
            this.DataBind();
        }
    }
    protected void dropdownlist1_selected(object sender,EventArgs e)
    {
        SqlDataAdapter dataAdapter = new SqlDataAdapter();
        DataSet ds = new DataSet();
        SqlConnection connection = new SqlConnection();
        connection.ConnectionString = WebConfigurationManager.ConnectionStrings["Pubs"].ConnectionString;
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Select * from City where StateId=@stateid", connection);
            command.Parameters.AddWithValue("@stateid", int.Parse(DropDownList1.SelectedItem.Value));
            dataAdapter = new SqlDataAdapter(command);
            dataAdapter.Fill(ds,"City");
            connection.Close();
        }
        catch(Exception ex) { }
        finally
        {
            connection.Close();
        }
        foreach (DataRow row in ds.Tables["City"].Rows)
        {
            ListItem ls = new ListItem();
            ls.Text = row["Cityname"].ToString();
            DropDownList2.Items.Add(ls);
        }
        this.DataBind();
    }
}