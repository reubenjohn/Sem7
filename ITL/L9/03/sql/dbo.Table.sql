﻿CREATE TABLE [dbo].[City]
(
	[StateId] INT NOT NULL PRIMARY KEY, 
    [CityName] VARCHAR(50) NOT NULL
)
