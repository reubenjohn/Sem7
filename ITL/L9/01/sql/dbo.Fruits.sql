﻿CREATE TABLE [dbo].[Fruits]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [FruitName] VARCHAR(50) NOT NULL, 
    [Cost] FLOAT NOT NULL 
)
