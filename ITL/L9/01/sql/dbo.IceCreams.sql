﻿CREATE TABLE [dbo].[IceCreams]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Name] VARCHAR(50) NOT NULL, 
    [Cost] FLOAT NOT NULL
)
