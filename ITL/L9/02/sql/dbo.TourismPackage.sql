﻿CREATE TABLE [dbo].[TourismPackage]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Place] VARCHAR(50) NOT NULL
)
