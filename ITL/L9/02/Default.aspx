﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ListBox ID="ListBox1" runat="server" DataSourceID="datasourceId1" DataTextField="Place" AutoPostBack="true" OnSelectedIndexChanged="listbox_selected"></asp:ListBox>
            <asp:SqlDataSource ID="datasourceId1" runat="server" ConnectionString="<%$ConnectionStrings:Pubs %>" SelectCommand="Select Place from TourismPackage"></asp:SqlDataSource>
        </div>
    </form>
</body>
</html>
