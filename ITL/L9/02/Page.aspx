﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Page.aspx.cs" Inherits="Page" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="true" DataSourceId="SqlDataSource1"></asp:GridView>
             <asp:SqlDataSource ID="SqlDataSource1" runat="server" SelectCommand="Select * from TourismPackage where Place=@val" ConnectionString="<%$ConnectionStrings:Pubs %>">
                 <SelectParameters>
                     <asp:QueryStringParameter Name="val" QueryStringField="val" Type="String" />
                 </SelectParameters>

             </asp:SqlDataSource>

        </div>
       
    </form>
</body>
</html>
