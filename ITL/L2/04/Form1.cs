﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _04
{
    public partial class Form1 : Form
    {
        Dictionary<String, float> costs;
        private float cost;
        private int quantity;

        public Form1()
        {
            InitializeComponent();
            costs = new Dictionary<String, float>();
            costs.Add("Yonex", 2000);
            costs.Add("2Silvers", 1000);
            costs.Add("Cosco", 800);
        }

        private void brandSelected(object sender, EventArgs e)
        {
            cost = costs[((RadioButton)sender).Text];
            Update();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            int.TryParse(((TextBox)sender).Text, out quantity);
            Update();
        }

        private void Update()
        {
            label3.Text = (quantity * cost).ToString();
        }
    }
}
