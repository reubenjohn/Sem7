﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _03
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        private void hardDiskChecked(object sender, EventArgs e)
        {
            label1.Text = ((RadioButton)sender).Text;
        }

        private void mobileChecked(object sender, EventArgs e)
        {
            label2.Text = ((RadioButton)sender).Text;
        }
    }
}
