﻿using System;
using System.Windows.Forms;

namespace ExampleForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double n1;
            double.TryParse(textBox1.Text, out n1);
            textBox2.Text = (n1 * 2).ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox2.Text = "";
        }
    }
}