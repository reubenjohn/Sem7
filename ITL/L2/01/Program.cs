﻿using System;
namespace Add_Num
{
    class Program
    {
        static void Main(string[] args)
        {
            int num1, num2;
            Console.WriteLine("Enter the numbers:");
            string n1 = Console.ReadLine(); // Readline() method returns string type
            string n2 = Console.ReadLine();
            int.TryParse(n1, out num1);
            int.TryParse(n2, out num2);
            Console.WriteLine("Enter the operation: ");
            int op = Console.Read();
            Console.ReadLine();

            try
            {
                Console.WriteLine("Result: {0}", getResult(num1, num2, op));
            }catch(ArgumentException e)
            {
                Console.WriteLine(e);
            }
            Console.Read(); // Read() accepts only single character 
        }

        private static int getResult(int n1, int n2, int op)
        {
            switch (op)
            {
                case '+':
                    return n1 + n2;
                case '-':
                    return n1 - n2;
                case '*':
                    return n1 * n2;
                case '/':
                    return n1 / n2;
                default:
                    throw new ArgumentException("Unknown operation");
            }
        }
    }
}