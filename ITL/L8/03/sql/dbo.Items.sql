﻿CREATE TABLE [dbo].[Items]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [flavour] VARCHAR(50) NOT NULL, 
    [price] INT NOT NULL, 
    [taste] VARCHAR(50) NOT NULL
)
