﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class _Default : System.Web.UI.Page
{
    SqlConnection con;

    protected void tryCommand(string sql, Action<SqlDataReader> queryCallback, Action<int> nonQueryCallback)
    {
        try
        {
            SqlCommand cmd = new SqlCommand(sql, con);
            if (queryCallback != null)
            {
                SqlDataReader reader = cmd.ExecuteReader();
                queryCallback.Invoke(reader);
                reader.Close();
            }
            if (nonQueryCallback != null)
            {
                int result = cmd.ExecuteNonQuery();
                nonQueryCallback.Invoke(result);
            }
        }
        catch (Exception err)
        {
            throw err;
        }
        finally
        {
            con.Close();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        con = new SqlConnection();
        con.ConnectionString = WebConfigurationManager.ConnectionStrings["Products"].ConnectionString;
        con.Open();
    }

    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {
        tryCommand("UPDATE Items SET price=" + TextBox1.Text + " WHERE flavour='vanila'", null, result =>
        {
            TextBox1.Text = "Updated " + result + " rows";
        });
    }
}