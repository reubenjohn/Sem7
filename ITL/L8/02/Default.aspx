﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Genre:
        <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="true">
            <asp:ListItem>comedy</asp:ListItem>
            <asp:ListItem>romance</asp:ListItem>
            <asp:ListItem>animated</asp:ListItem>
        </asp:DropDownList>
            <br />
            <asp:ListBox ID="ListBox1" AutoPostBack="true" runat="server" DataSourceID="SqlDataSource1" DataTextField="name" DataValueField="Id" OnSelectedIndexChanged="ListBox1_SelectedIndexChanged"></asp:ListBox>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Test2 %>" SelectCommand="SELECT * FROM [Legends] WHERE ([category] = @category)">
                <SelectParameters>
                    <asp:ControlParameter ControlID="DropDownList1" DefaultValue="comedy" Name="category" PropertyName="SelectedValue" Type="String" />
                </SelectParameters>
            </asp:SqlDataSource>

            <br />
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:Test2 %>" SelectCommand="SELECT name, age FROM [Legends] WHERE ([id] = @id)">
                <SelectParameters>
                    <asp:ControlParameter ControlID="ListBox1" Name="id" PropertyName="Text" Type="String" />
                </SelectParameters>
            </asp:SqlDataSource>
            <textarea id="TextArea1" runat="server" cols="20" name="S1" rows="2"></textarea>
        </div>
    </form>
</body>
</html>
