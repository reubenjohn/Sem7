﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class _Default : System.Web.UI.Page
{
    protected void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = (SqlDataSource2.Select(DataSourceSelectArguments.Empty) as DataView)
            .ToTable() as DataTable;
        TextArea1.InnerText = dt.Rows[0][0] + " " + dt.Rows[0][1];
    }
}