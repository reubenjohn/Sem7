﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    SqlConnection con;
    DataSet ds;

    protected void tryCommand(string sql, Action<SqlDataReader> queryCallback, Action<int> nonQueryCallback)
    {
        try
        {
            SqlCommand cmd = new SqlCommand(sql, con);
            if (queryCallback != null)
            {
                SqlDataReader reader = cmd.ExecuteReader();
                queryCallback.Invoke(reader);
                reader.Close();
            }
            if (nonQueryCallback != null)
            {
                int result = cmd.ExecuteNonQuery();
                nonQueryCallback.Invoke(result);
            }
        }
        catch (Exception err)
        {
            Label1.Text = "DB operation failed: ";
            throw err;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["con"] = con = new SqlConnection();
            con.ConnectionString = WebConfigurationManager.ConnectionStrings["Staff"].ConnectionString;
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Staff", con);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            ds = new DataSet();
            adapter.Fill(ds, "Staff");
            foreach (DataRow row in ds.Tables["Staff"].Rows)
            {
                ListItem item = new ListItem(((int)row["StaffID"]).ToString());
                staffIDs.Items.Add(item);
            }
            Session["ds"] = ds;
        }
        con = (SqlConnection)Session["con"];
        ds = (DataSet)Session["ds"];
    }

    protected void staffID_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable table = ds.Tables["Staff"];
        foreach (DataRow row in table.Rows)
        {
            if (String.Equals((int)row["StaffID"], int.Parse(staffIDs.SelectedItem.Text)))
            {
                details.Text = "Name: " + row["FirstName"] + " " + row["LastName"]
                    + ", DNo: " + row["DNo"] + ", Street: " + row["Street"]
                    + ", City: " + row["City"] + ", State: " + row["State"]
                    + ", ZipCode: " + row["ZipCode"];
                break;
            }
        }
        Label1.Text = "";
    }

    protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
    {
        tryCommand("UPDATE Staff SET City='" + cities.SelectedItem.Text + "' WHERE StaffID='" + staffIDs.SelectedValue + "'", null, result =>
             {
                 Label1.Text = "Successfully updated City";
             });
    }
}