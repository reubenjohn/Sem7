﻿CREATE PROCEDURE [dbo].[Procedure]
AS
	INSERT INTO [dbo].[Staff] ([StaffID], [FirstName], [LastName], [DNo], [Street], [City], [State], [ZipCode]) VALUES (1, N'Abc', N'Xyz', 1234, N'4', N'Udupi', N'Karnataka', CAST(690108 AS Decimal(18, 0)));
	INSERT INTO [dbo].[Staff] ([StaffID], [FirstName], [LastName], [DNo], [Street], [City], [State], [ZipCode]) VALUES (2, N'Abc2', N'Xyz2', 1235, N'5', N'Udupi', N'Karnataka', CAST(690108 AS Decimal(18, 0)));

RETURN 0
