﻿CREATE TABLE [dbo].[Lives]
(
	[name] VARCHAR(50) NOT NULL PRIMARY KEY, 
    [street] VARCHAR(50) NOT NULL, 
    [city] VARCHAR(50) NOT NULL
)
