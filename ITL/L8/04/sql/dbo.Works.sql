﻿CREATE TABLE [dbo].[Works]
(
	[name] VARCHAR(50) NOT NULL PRIMARY KEY, 
    [company] VARCHAR(50) NOT NULL, 
    [salary] FLOAT NOT NULL
)
