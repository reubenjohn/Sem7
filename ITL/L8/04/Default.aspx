﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Name:
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <br />
            Company:
        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
            <br />
            Salary:
        <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
            <br />
            <asp:Button ID="Button1" runat="server" Text="Insert" OnClick="Button1_Click" />
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:P4 %>" InsertCommand="INSERT INTO [Works] VALUES (@name, @company, @salary)">
                <InsertParameters>
                    <asp:ControlParameter Name="name" ControlID="TextBox1" PropertyName="Text" Type="String" />
                    <asp:ControlParameter Name="company" ControlID="TextBox2" PropertyName="Text" Type="String" />
                    <asp:ControlParameter Name="salary" ControlID="TextBox3" PropertyName="Text" Type="Double" />
                </InsertParameters>
            </asp:SqlDataSource>

            <br />
            <br />
            Employees of:
        <asp:TextBox ID="TextBox4" runat="server" AutoPostBack="true"></asp:TextBox>
            <br />
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="name" DataSourceID="SqlDataSource1">
                <Columns>
                    <asp:BoundField DataField="name" HeaderText="name" ReadOnly="True"/>
                    <asp:BoundField DataField="city" HeaderText="city" ReadOnly="True"/>
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:P4 %>"
                SelectCommand="SELECT [Works].[name], [Lives].[city] FROM [Works], [Lives] WHERE ([company] = @company and [Works].[name]=[Lives].[name])">
                <SelectParameters>
                    <asp:ControlParameter ControlID="TextBox4" Name="company" PropertyName="Text" Type="String" />
                </SelectParameters>
            </asp:SqlDataSource>

        </div>
    </form>
</body>
</html>
