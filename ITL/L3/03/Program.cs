﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace _03
{
    class Program
    {
        private delegate decimal TaxCalculator(decimal salary);
        private static decimal TaxIndia(decimal salary) { return salary * 0.1M; }
        private static decimal TaxUS(decimal salary) { return salary * 0.2M; }
        static void Main(string[] args)
        {
            Write("Salary: ");
            decimal.TryParse(ReadLine(), out decimal salary);
            Write("Tax calculator: [India(0)|US(~0)]: ");
            short.TryParse(ReadLine(), out short selection);
            TaxCalculator taxCalculator = selection == 0 ? (TaxCalculator)TaxIndia : TaxUS;
            WriteLine("Tax: " + taxCalculator(salary));
            ReadLine();
        }
    }
}
