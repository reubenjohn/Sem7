﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace _02
{
    public delegate void PriceChangedEventHandler(decimal price);
    class Program
    {
        static void Main(string[] args)
        {
            Write("Enter Name and Price: ");
            string name = ReadLine();
            decimal.TryParse(ReadLine(), out decimal price);
            Item item = new Item()
            {
                Name = name,
                Price = 0
            };
            WriteLine("(Old price: " + item.Price + ")");
            item.PriceChanged += OnPriceChanged;
            item.Price = price;
            ReadLine();
        }
        private static void OnPriceChanged(decimal price)
        {
            WriteLine("Price changed to: " + price);
        }
    }

    class Item
    {
        public event PriceChangedEventHandler PriceChanged;
        public string Name { get; set; }
        private decimal price;
        public decimal Price
        {
            get { return price; }
            set { price = value; PriceChanged?.Invoke(value); }
        }
    }
}
