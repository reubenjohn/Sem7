﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace _04
{
    class Program
    {
        static void Main(string[] args)
        {
            EBBill ebBill = new EBBill();
            WriteLine("Metre Rent; Owner Name; House Number; Units Consumed : ");
            float.TryParse(ReadLine(), out EBBill.MetreRent);
            ebBill.OwnerName = ReadLine();
            int.TryParse(ReadLine(), out ebBill.HouseNumber);
            float.TryParse(ReadLine(), out ebBill.UnitsConsumed);
            WriteLine(ebBill);
            ReadLine();
        }
    }

    class EBBill
    {
        public static float MetreRent;

        public string OwnerName { get; set; }
        public int HouseNumber;
        public float UnitsConsumed;

        public float Bill { get { return 1.2f * UnitsConsumed + MetreRent; } }

        public override string ToString()
        {
            return "Name: " + OwnerName + ", Bill: " + Bill;
        }
    }
}
