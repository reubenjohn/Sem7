﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace _01
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();
            int[] randArr = Enumerable
                .Repeat(0, 5)
                .Select(i => random.Next(0, 20)).ToArray();
            WriteLine("Input:\t" + string.Join("\t", randArr));
            randArr = randArr
                .OrderBy(i => i).ToArray();
            WriteLine("Output:\t" + string.Join("\t", randArr));
            ReadLine();
        }
    }
}
