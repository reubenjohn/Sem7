﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        Name:
        <asp:TextBox ID="Label1" runat="server"></asp:TextBox>
        <br />
        Designation:
        <asp:DropDownList ID="DropDownList1" runat="server">
            <asp:ListItem Text="Faculty" />
            <asp:ListItem Text="Student" />
        </asp:DropDownList>
        <br />
        <asp:Button ID="btn" Text="Show Courses" OnClick="btn_Click" runat="server"/>
    </form>
</body>
</html>
