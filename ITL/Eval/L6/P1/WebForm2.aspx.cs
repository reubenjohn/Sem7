﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebForm2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Label1.Text = "Welcome " + Request.QueryString["name"] + " to Coursera";
            int designationIndex;
            if (!int.TryParse(Request.Cookies["Pref"]["designation"], out designationIndex))
            {
                designationIndex = -1;
            }
            switch (designationIndex)
            {
                case 0:
                    Label2.Text = "Machine Learning<br>";
                    Label2.Text += "Big Data Analytics<br>";
                    Label2.Text += "Data Mining<br>";
                    break;
                case 1:
                    Label2.Text = "Algorithms<br>";
                    Label2.Text += "Data Structures<br>";
                    Label2.Text += "Operating Systems<br>";
                    break;
                default:
                    Label2.Text = "Unknown";
                    break;
            }
        }
    }
}