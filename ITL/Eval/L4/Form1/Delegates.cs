﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Form1
{
    public delegate string GetCheck(string input);
    class Delegates
    {
        public event GetCheck Checker;

        public GetCheck getResult()
        {
            return Checker;
        }
        public void setResult(GetCheck check)
        {
            this.Checker = check;
        }
    }
}
