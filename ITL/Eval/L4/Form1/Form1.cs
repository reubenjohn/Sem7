﻿using System;
using System.Windows.Forms;
using static Form1.Delegates;

namespace Form1
{
    public partial class Form1 : Form
    {
        Delegates delegates;

        public Form1()
        {
            InitializeComponent();
            delegates = new Delegates();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (
                    delegates.getResult() != null)
            {
                label1.Text = delegates.getResult().Invoke(textBox1.Text);
            }
            else
            {
                label1.Text = "Check not specified";
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (((ComboBox)sender).SelectedIndex)
            {
                case 0:
                    delegates.setResult(Even);
                    break;
                case 1:
                    delegates.setResult(Odd);
                    break;
                case 2:
                    delegates.setResult(Divisible10);
                    break;
                default:
                    label1.Text = "Unknown Check!";
                    break;
            }
        }

        public string Even(string input)
        {
            int i = 1;
            int.TryParse(input, out i);
            if (i % 2 == 0)
            {
                return "Is even";
            }
            else
            {
                return "Is not even";
            }
        }
        public string Odd(string input)
        {
            int i = 1;
            int.TryParse(input, out i);
            if (i % 2 == 1)
            {
                return "Is odd";
            }
            else
            {
                return "Is not odd";
            }
        }
        public string Divisible10(string input)
        {
            int i = 1;
            int.TryParse(input, out i);
            if (i % 10 == 0)
            {
                return "Is divisible by 10";
            }
            else
            {
                return "Is not divisible by 10";
            }
        }
    }
}
