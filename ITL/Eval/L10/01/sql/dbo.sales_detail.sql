﻿CREATE TABLE [dbo].[sales_detail]
(
	[sales_id] INT NOT NULL PRIMARY KEY, 
    [item_name] VARCHAR(255) NOT NULL, 
    [item_amt] NUMERIC(10, 2) NOT NULL, 
    [quantity] NUMERIC(8, 3) NOT NULL
)
