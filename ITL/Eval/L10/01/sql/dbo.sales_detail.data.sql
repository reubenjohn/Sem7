INSERT INTO [dbo].[sales_detail] ([sales_id], [item_name], [item_amt], [quantity]) VALUES (1, N'qwe', CAST(100.00 AS Decimal(10, 2)), CAST(1.000 AS Decimal(8, 3)))
INSERT INTO [dbo].[sales_detail] ([sales_id], [item_name], [item_amt], [quantity]) VALUES (1, N'qwe2', CAST(200.00 AS Decimal(10, 2)), CAST(2.000 AS Decimal(8, 3)))
INSERT INTO [dbo].[sales_detail] ([sales_id], [item_name], [item_amt], [quantity]) VALUES (2, N'zxc', CAST(100.00 AS Decimal(10, 2)), CAST(1.000 AS Decimal(8, 3)))
INSERT INTO [dbo].[sales_detail] ([sales_id], [item_name], [item_amt], [quantity]) VALUES (2, N'zxc2', CAST(100 AS Decimal(10, 2)), CAST(2 AS Decimal(8, 3)))
