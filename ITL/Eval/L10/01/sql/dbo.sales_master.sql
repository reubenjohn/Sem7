﻿CREATE TABLE [dbo].[sales_master]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [date] DATETIME NOT NULL, 
    [customer_name] VARCHAR(50) NOT NULL, 
    [total_amt] NUMERIC(12) NOT NULL
)
