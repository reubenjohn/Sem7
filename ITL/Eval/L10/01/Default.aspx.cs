﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string conStr = WebConfigurationManager.ConnectionStrings["eval10"].ConnectionString;

            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand("select * from sales_master", con);

            SqlDataAdapter adap = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();

            try
            {
                con.Open();
                adap.Fill(ds, "sales_master");

                data.InnerHtml = "";

                foreach (DataRow row in ds.Tables["sales_master"].Rows)
                {
                    data.InnerHtml += "<b>" + row["id"] + " " + row["customer_name"] + " " + row["total_amt"] + "</b>\n<br>\n";

                    SqlCommand cmd2 = new SqlCommand("select * from sales_detail where sales_id=" + row["id"], con);
                    SqlDataReader reader = cmd2.ExecuteReader();

                    while (reader.Read())
                    {
                        //data.InnerHtml += reader["item_name"] + ": " + (((decimal)reader["item_amt"]) * ((decimal)reader["quantity"])).ToString() + "<br>\n";
                        data.InnerHtml += reader["item_name"] + ": " + (((decimal)reader["quantity"])).ToString() + "<br>\n";
                    }
                    reader.Close();
                }
                con.Close();
            }
            catch (Exception ex) { throw ex; }
        }
    }
}