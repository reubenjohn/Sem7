﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" defaultbutton="submit" runat="server">
        Election Form<br />
        Candidate<br />
        <asp:DropDownList ID="candidates" runat="server" AutoPostBack="true" OnSelectedIndexChanged="Submit_Click">
            <asp:ListItem Value="0">Candidate 1</asp:ListItem>
            <asp:ListItem Value="1">Candidate 2</asp:ListItem>
            <asp:ListItem Value="2">Candidate 3</asp:ListItem>
        </asp:DropDownList>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="None" ControlToValidate="candidates" runat="server" ErrorMessage="Required candidate"></asp:RequiredFieldValidator>
        <br />
        House<asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="None" ControlToValidate="houses" runat="server" ErrorMessage="Required house"></asp:RequiredFieldValidator>
        <asp:RadioButtonList ID="houses" runat="server">
            <asp:ListItem>1</asp:ListItem>
            <asp:ListItem>2</asp:ListItem>
            <asp:ListItem>3</asp:ListItem>
            <asp:ListItem>4</asp:ListItem>
        </asp:RadioButtonList>
        Class<br />
        <asp:TextBox ID="class" runat="server" AutoPostBack="true"/>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="None" ControlToValidate="class" runat="server" ErrorMessage="Required class"></asp:RequiredFieldValidator>
        <asp:RangeValidator ID="RangeValidator1" Display="None" MinimumValue="6" MaximumValue="12" Type="Integer" ControlToValidate="class" runat="server" ErrorMessage="Out of bounds (6-12)"></asp:RangeValidator>
        <br />
        Email<br />
        <asp:TextBox ID="email" runat="server"></asp:TextBox>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="^[a-zA-Z0-9_.]+@[a-zA-Z0-9_.]+\.(com|co.in)$" Display="None" ControlToValidate="email" ErrorMessage="Irregular email"></asp:RegularExpressionValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="None" ControlToValidate="email" runat="server" ErrorMessage="Required email"></asp:RequiredFieldValidator>
        <br />
        Contact<br />
        <asp:TextBox ID="contact" runat="server" AutoPostBack="true"></asp:TextBox>
        <asp:CustomValidator ID="CustomValidator1" Display="None" ControlToValidate="contact" runat="server" ErrorMessage="Invalid contact"></asp:CustomValidator>
        <br />
        <asp:Button ID="submit" runat="server" Text="Submit" OnClick="Submit_Click" />
        <asp:Label ID="result" runat="server" />
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
    </form>
</body>
</html>
